<?php
echo '0:        '.(boolval(0) ? 'true' : 'false')."<br />\n";
echo '42:       '.(boolval(42) ? 'true' : 'false')."<br />\n";
echo '0.0:      '.(boolval(0.0) ? 'true' : 'false')."<br />\n";
echo '4.2:      '.(boolval(4.2) ? 'true' : 'false')."<br />\n";
echo '"":       '.(boolval("") ? 'true' : 'false')."<br />\n";
echo '"string": '.(boolval("string") ? 'true' : 'false')."<br />\n";
echo '"0":      '.(boolval("0") ? 'true' : 'false')."<br />\n";
echo '"1":      '.(boolval("1") ? 'true' : 'false')."<br />\n";
echo '[1, 2]:   '.(boolval([1, 2]) ? 'true' : 'false')."<br />\n";
echo '[]:       '.(boolval([]) ? 'true' : 'false')."<br />\n";
echo '"ranju":  '.(boolval("ranju") ? 'true' : 'false'). "<br /> \n";


