-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2016 at 08:42 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gender`
--

-- --------------------------------------------------------

--
-- Table structure for table `td_gender`
--

CREATE TABLE IF NOT EXISTS `td_gender` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `newname` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_gender`
--

INSERT INTO `td_gender` (`id`, `name`, `newname`, `sex`, `unique_id`) VALUES
(56, 'omargfdgs', 'farukgfd', 'Female', ' 57870eebeecfc '),
(57, 'korim', 'khan', 'Male', ' 57870f0743cbb '),
(58, 'mohona', 'khatun', 'Female', ' 57870f1c40705 '),
(59, 'badhon', 'khatun', 'Female', ' 57870f5047044 '),
(67, 'vbcx', 'bvcx', 'Male', ' 578723e759f49 '),
(71, 'gjhhg', 'gfdsgsfdgfds', 'Male', ' 578725e370cb9 ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `td_gender`
--
ALTER TABLE `td_gender`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `td_gender`
--
ALTER TABLE `td_gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
