<?php
include_once '../../../../src/bitm/seip128727/actor/actor.php';

$object = new Actor();


$onedata = $object->prepare($_GET)->show();

if (isset($onedata) && !empty($onedata)) {
    ?>
    <a href="index.php">Back to actor list</a>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Unique_id</th>
        </tr>  
        <tr>
            <td><?php echo $onedata['id']; ?></td>
            <td><?php echo $onedata['title']; ?></td>
            <td><?php echo $onedata['unique_id']; ?></td>
        </tr>
    </table>
<?php
} else {
    $_SESSION['Err_Msg'] = "Not found Something going wrong! <br>" . "<a href='create.php'> Add New Item</a>";
    header('location:errors.php');
}
?>

