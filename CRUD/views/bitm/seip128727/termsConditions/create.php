<?php
//Show success message after validation
session_start();
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}
if (isset($_SESSION['formData']) && !empty($_SESSION['formData'])){

}
?>
<fieldset>
    <legend>Semester Enrolment</legend>
    <form action="store.php" method="post">
        <label>Name</label>
        <input type="text" name="name" placeholder="Student Name" value="<?php
        if (isset($_SESSION['formData']['name'])){
            echo $_SESSION['formData']['name'];
            unset($_SESSION['formData']['name']);
        }
        ?>">
        <?php  // Required field validation
        if (!empty($_SESSION['nameEmpty'])){
            echo $_SESSION['nameEmpty'];
            unset($_SESSION['nameEmpty']);
        }
        ?>
        <br/><br/>
        <label>Semester :</label>
        <select name="semester">
            <option value="">Select Semester</option>
            <option value="1">Semester 1</option>
            <option value="2">Semester 2</option>
            <option value="3">Semester 3</option>
        </select>
        <?php  // Required field validation
        if (!empty($_SESSION['semesterEmpty'])){
            echo $_SESSION['semesterEmpty'];
            unset($_SESSION['semesterEmpty']);
        }
        ?>
        <br/><br/>
        <input type="checkbox" name="offer" value="1">Check to Get Your Semester Offer!<br/><br/><br/>
        <input type="submit">
    </form>
</fieldset><br>
<a href="index.php">Back To List</a>