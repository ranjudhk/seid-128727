<?php
include_once '../../../../src/bitm/seip128727/termsConditions/terms.php';
//include_once ("../vendor/autoload.php");

//use App\Terms;

$obj = new Terms();
$data = $obj->prepare($_GET)->show();
if (isset($data) && !empty($data)){ //unauthorised Validation through URL
?>
<!--show data in a Table-->
<table border="1" cellpadding="5">
    <tr>
        <th>Name</th>
        <th>Semester</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Waiver</th>
        <th>Total</th>
    </tr>
    <tr>
        <td><?php echo $data['name']; ?></td>
        <td><?php echo $data['semester']; ?></td>
        <th><?php if ($data['offer'] == 1){ echo "Yes"; ?>
                
            <?php } else{ echo "No"; } ?>
        </th>
        <td><?php echo $data['cost']; ?></td>
        <td><?php echo $data['waiver']; ?></td>
        <td><?php echo $data['total']; ?></td>
    </tr>
</table>
<a href="index.php">Back To List</a>
<?php }else{
    $_SESSION['Err_Msg'] = "Invalid ID!";
    header("location: error.php");
}
?>