<?php
include_once '../../../../src/bitm/seip128727/termsConditions/terms.php';
//include_once ("../vendor/autoload.php");

//use App\Terms;

$obj = new Terms();
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $obj->prepare($_POST)->store();
}else{
    $_SESSION["Err_Msg"] = "You are not authorized to access this page!";
    header("location: error.php");
}
