<?php
include_once '../../../../src/bitm/seip128727/termsConditions/terms.php';
//include_once ('../vendor/autoload.php');
//use App\Terms;

$obj = new Terms();
$allData = $obj->index();
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) //validate the delete message
    echo $_SESSION['msg'];
unset($_SESSION['msg']);
?>
<h2>Semester Enroll and Waiver Information:</h2>
<table border="1" cellpadding="5">
    <tr>
        <th>SL</th>
        <th>Name</th>
        <th>Semester</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Waiver</th>
        <th>Total</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($allData) && !empty($allData)) {
        foreach ($allData as $item) {
            ?>
            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $item['name'] ?></td>
                <td><?php echo $item['semester'] ?></td>
                <th>
                    <?php if ($item['offer'] == 1) {
                        echo "Yes";
                    } else {
                        echo "No";
                    }
                    ?>
                </th>
                <td><?php echo $item['cost'] ?></td>
                <td><?php if($item['offer']==1){ echo $item['waiver']; } else{echo "N/A";} ?></td>
                <td><?php echo $item['total'] ?></td>
                <td><a href="show.php?id=<?php echo $item['uid'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $item['uid'] ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $item['uid'] ?>">Delete</a></td>
            </tr>
    <?php }
} else { ?>
        <tr>
            <th colspan="8">No data available</th>
        </tr>
    <?php
}
?>
</table>
<a href="create.php">Insert More >></a>