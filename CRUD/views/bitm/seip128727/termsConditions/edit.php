<?php
include_once '../../../../src/bitm/seip128727/termsConditions/terms.php';
//include_once ("../vendor/autoload.php");

//use App\Terms;

$obj = new Terms();
$_SESSION['id'] = $_GET['id']; //send id to update page
$data = $obj->prepare($_GET)->show();
if (isset($data) && !empty($data)){ //unauthorised Validation through URL
?>
<fieldset>
    <legend>Update Semester Enrolment</legend>
    <form action="update.php" method="post">
        <label>Name</label>
        <input type="text" name="name" value="<?php echo $data['name'];?>" autofocus><br/><br/>
        <label>Semester :</label>
        <select name="semester">
            <option value="">Select Semester</option>
            <option value="1">Semester 1</option>
            <option value="2">Semester 2</option>
            <option value="3">Semester 3</option>
        </select><br/><br/>
        <?php
        if ($data['offer'] == 1){ ?>
            <input type="checkbox" name="offer" value="1" checked>Check to Get Your Semester Offer!
        <?php } else { ?>
            <input type="checkbox" name="offer" value="1">Check to Get Your Semester Offer!
        <?php }
        ?><br/><br/><br/>
        <input type="submit" value="Update">
    </form>
</fieldset>
<?php }else{
    $_SESSION['Err_Msg'] = "Invalid ID!";
    header("location: error.php");
}
?>