<?php
include_once '../../../../src/bitm/seip128727/student/Student.php';

$object = new Student();


$onedata = $object->prepare($_GET)->show();

if(isset($onedata) && !empty($onedata)){

?>
<a href="index.php">Back to student's list</a>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Semester</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Weaver</th>
        <th>Total</th>
    </tr>  
    <tr>
        <td><?php echo $onedata['id'] ;?></td>
        <td><?php echo $onedata['name'] ;?></td>
        <td><?php echo $onedata['semester'] ;?></td>
        <td><?php echo $onedata['offer'] ;?></td>
        <td><?php echo $onedata['cost'] ;?></td>
        <td><?php echo $onedata['weaver'] ;?></td>
        <td><?php echo $onedata['total'] ;?></td>
    </tr>
    </table>
<?php  } else {
    $_SESSION['Err_Msg'] = "Not found Something going wrong! <br>" . "<a href='create.php'> Add New Item</a>";
    header('location:errors.php');
} ?>

