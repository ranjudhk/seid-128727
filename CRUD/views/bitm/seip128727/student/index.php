<?php
include_once '../../../../src/bitm/seip128727/student/Student.php';
?>
<a href="../../../../index.php">List of Project</a> <br/> <br/>
<a href="create.php">Put Student Info</a> <br/> <br/>
<?php
$obj = new Student();
$Alldata = $obj->index();



if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
<head>
    <title>Student</title>
</head>
<body>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Name</th>
        <th>Semester</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Weaver</th>
        <th>Total</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($Alldata) && !empty($Alldata)) {

        foreach ($Alldata as $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $Singledata['name'] ?></td>
                <td><?php echo $Singledata['semester'] ?></td>
                <td><?php echo $Singledata['offer'] ?></td>
                <td><?php echo $Singledata['cost'] ?></td>
                <td><?php echo $Singledata['weaver'] ?></td>
                <td><?php echo $Singledata['total'] ?></td>

            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="7">
                No student info available
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
