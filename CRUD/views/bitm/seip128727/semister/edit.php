<?php
require_once '../../../../src/bitm/seip128727/semister/Semister.php';

//use SemisterApp\bitm\seip131181\semister\Semister;

$objEdit = new Semister();

$data = $objEdit->prepare($_GET)->show();
?>
<html>
    <head>
        <title>Create</title>
        
    </head>
    <body>

        <div class="container">
            <fieldset>
                <legend>Edit and Update</legend>
                <form class="form-inline" method="POST" action="store.php">


                    <div class="form-group">
                        <label >Your Name:</label>
                        <input type="text" class="form-control" name="fname" value = "<?php echo $data['name']; ?>">
                    </div>

                    <div class="form-group">
                        <label >Select Semester:</label>
                        <select class="form-control" name = "semister">
                            <option value="1st_Sem">1st Sem</option>
                            <option value="2nd_Sem">2nd Sem</option>
                            <option value="3rd_Sem">3rd Sem</option>
                        </select>
                    </div>


                    <div class="checkbox">
                        <label><input type="checkbox" name="offer" value="Offered">Get Offer</label>
                    </div>


                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </fieldset>
            <p><a href="index.php">Show List</a></p>
        </div>  <!--container -->


    </body>
</html>

