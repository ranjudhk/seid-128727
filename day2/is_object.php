<?php

function get_subjects($obj_name)  
{  
if(!is_object($obj_name))  
{  
return(false);  
}  
return($obj_name->subjects);  
}  
$obj_name = new stdClass;  
$obj_name->subjects = Array('Physics', 'Chemistry', 'Mathematics');  
var_dump(get_subjects(NULL));  
var_dump(get_subjects($obj_name)); 




echo "<br />";

//php.net
// Declare a simple function to return an 
// array from our object
function get_students($obj)
{
    if (!is_object($obj)) {
        return false;
    }

    return $obj->students;
}

// Declare a new class instance and fill up 
// some values
$obj = new stdClass();
$obj->students = array('Kalle', 'Ross', 'Felipe');

var_dump(get_students(null));
var_dump(get_students($obj));